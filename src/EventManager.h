
#ifndef _SZYREK_EVENT_MANAGER
#define _SZYREK_EVENT_MANAGER
class EventManager
{
public:
    EventManager();
    ~EventManager();
    static EventManager getInstance();
    template <class T> void handleEvent(T event);
private:
    static EventManager _instance;

};
#endif
