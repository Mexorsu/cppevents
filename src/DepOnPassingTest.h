#ifndef DEPONPASSINGTESTUT_H
#define DEPONPASSINGTESTUT_H
#include"UT.h"
class DepOnPassingTestUT: public UT
{
public:
    DepOnPassingTestUT();
    ~DepOnPassingTestUT();
private:
    void test();
};
#endif /* DEPONPASSINGTESTUT_H */
