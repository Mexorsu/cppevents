#include"TestResult.h"

TestResult TestResult::TEST_OK;

void TestResult::addError(TestError err)
{
    _errors.push_back(err);
    passed = false;
    failed = true;
}
void TestResult::addAllErrors(TestResult r)
{
    for (auto error: r.getErrors())
    {
       addError(error);
    }
}

std::vector<TestError> TestResult::getErrors()
{
	return this->_errors;
}
