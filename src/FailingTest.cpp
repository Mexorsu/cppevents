#include"FailingTest.h"
FailingTestUT::FailingTestUT(): UT("FailingTest"){}
FailingTestUT::~FailingTestUT()
{
}
void FailingTestUT::test()
{
    TestResult itsCool;
    TestError err = TestError("Ooops- there's an error!");
    TestError why3 = TestError("It's ok, I rly should fail- it's a metatest :D");
    TestError why2 = TestError("Because thats just how it is I guess.", why3);
    TestError why1 = TestError("Why? I don't really know.", why2);
    err.setCause(why1);
    fail(err);
    fail(TestError("And there's also an error here"));
}
