#ifndef UT_H
#define UT_H
#include<vector>
#include<string>
#include<memory>
#include"TestError.h"
#include"TestResult.h"

class UT
{
public:
	UT();
	UT(std::string testName);
    UT(std::vector<std::shared_ptr<UT>> _dependencies);
    UT(std::vector<std::shared_ptr<UT>> _dependencies, std::string testName);
    virtual ~UT();
    bool wasRan();
    TestResult getResult();
    void run();
    void printResults();
private:
    std::string _name;
    bool _ran = false;
    TestResult _result;
    virtual void test() = 0;
protected:
    std::vector<std::shared_ptr<UT>> _dependencies;
    void fail(TestError err);
    void assertTrue(bool sth);
    void assertFalse(bool sth);
    void assertTrue(bool sth, std::string err);
    void assertFalse(bool sth, std::string err);
    void assertEquals(bool sth);
    void assertTrue(bool sth, std::string filename, unsigned short num);
    void assertFalse(bool sth, std::string filename, unsigned short num);
    void assertTrue(bool sth, std::string err, std::string filename, unsigned short num);
    void assertFalse(bool sth, std::string err, std::string filename, unsigned short num);
    void assertEquals(bool sth, std::string filename, unsigned short num);


};
#endif /* UT_H */
