#ifndef PASSINGTESTUT_H
#define PASSINGTESTUT_H
#include"UT.h"
class PassingTestUT: public UT
{
public:
    PassingTestUT();
    ~PassingTestUT();
private:
    void test();
};
#endif /* PASSINGTESTUT_H */
