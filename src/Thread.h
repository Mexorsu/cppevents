#ifndef __SZYREK_THREAD
#define __SZYREK_THREAD
class Thread
{
public:
    virtual void start() = 0;
    virtual ~Thread(){};
    virtual void join() = 0;
    virtual void setRunnable(void(*f)()) = 0;
    virtual bool isRunning() = 0;
    virtual void run() = 0;
};
#endif
