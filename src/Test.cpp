#include"UTExecutor.h"
#include"TestsTest.h"
#include"NewTest.h"

int main(int argc, char* argv[])
{
    UTExecutor exec;
    exec.registerTest(std::make_shared<TestsTest>());
    exec.registerTest(std::make_shared<NewTest>());
    exec.run();
    return 0;
}
