#include"TestError.h"
#include<sstream>
#include<iostream>
#include<memory>

TestError::TestError(std::string description, TestError cause) :   _description(description)
{
	this->_cause = std::make_shared<TestError>(cause);
	this->_caused = true;
}

TestError::TestError(std::string description) : _description(description),
                                     _caused(false){}

std::string TestError::getDescription()
{
    return this->_description;
}

std::shared_ptr<TestError> TestError::getCause()
{
    return this->_cause;
}

void TestError::setCause(TestError another)
{
    this->_cause = std::make_shared<TestError>(another);
    this->_caused = true;
}

std::string TestError::getStackTrace()
{
    return getStackTrace(0);
}

std::string TestError::getStackTrace(unsigned short depth)
{
    std::stringstream spacer;
    for (int i = 0; i < depth; i++)
    {
        spacer << "--";
    }
    std::stringstream ss;
    ss << "|" << spacer.str() << "ERROR: " << _description << std::endl;
    if (this->_caused)
    {
        ss << this->_cause->getStackTrace(depth+1);
    }
    return ss.str();
}
