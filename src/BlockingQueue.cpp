#ifndef _MYGUARD_BLOCKING_QUEUE
#define _MYGUARD_BLOCKING_QUEUE
#include<deque>
#include<iostream>
#include<mutex>
#include<condition_variable>


template <class T>
class BlockingQueue
{
public:
    BlockingQueue(int size)
    {
        _maxSize = size;
    }
    ~BlockingQueue()
    {
    }
    void push(T element)
    {
        std::unique_lock<std::mutex> lock(_dataMutex);
        if (isFull())
        {
            _isNotFullCondition.wait(lock);
        }
        this->_data.push_back(element);
        lock.unlock();
        _isNotEmptyCondition.notify_all();
    }
    T pop()
    {
        std::unique_lock<std::mutex> lock(_dataMutex);
        if (isFull())
        {
            _isNotEmptyCondition.wait(lock);
        }
        T result = this->_data.front();
        this->_data.pop_front();
        lock.unlock();
        _isNotFullCondition.notify_all();
        return result;
    }
    int size()
    {
        return this->_data.size();
    }
    bool isEmpty()
    {
        bool result = this->_data.empty();
        return result;
    }
    bool isFull()
    {
        bool result = (this->_data.size()==this->_data.max_size());
        return result;
    }
private:
    int _maxSize;
    std::deque<T> _data;
    std::mutex _dataMutex;
    std::condition_variable _isNotEmptyCondition;
    std::condition_variable _isNotFullCondition;
};
#endif
