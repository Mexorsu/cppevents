#include"UT.h"
#include<sstream>
#include<vector>
#include<iostream>

UT::UT(): _name(""), _dependencies(std::vector<std::shared_ptr<UT>>())
{
    std::cerr << "Test not pre-compiled!" << std::endl;
    throw "Test not pre-compiled!";
}
UT::UT(std::string testName): _name(testName), _dependencies(std::vector<std::shared_ptr<UT>>()){}

UT::UT(std::vector<std::shared_ptr<UT>> dependencies) : _name(""), _dependencies(dependencies)
{
    std::cerr << "Test not pre-compiled!" << std::endl;
    throw "Test not pre-compiled!";
}
UT::UT(std::vector<std::shared_ptr<UT>> dependencies, std::string testName) : _name(testName), _dependencies(dependencies){}

UT::~UT(){}

void UT::run()
{
    // if the test was ran already, just
    // return the results
    if (this->wasRan())
    {
        return;
    }
    // first run all the tests we depend on
    for (auto dependency: _dependencies)
    {
        dependency->run();
        if (dependency->getResult().failed)
        {
        	std::stringstream ss;
        	ss << this->_name << " did not run due to failure of " << dependency->_name;
            TestError dependencyError(ss.str());
            _result.addError(dependencyError);
        }
    }
    // if we've not failed yet due to dependency
    // tests faliures, run my own test() method
    if (_result.passed)
    {
        this->test();
    }
    this->_ran = true;
}

void UT::printResults()
{
    TestResult theResult = getResult();
    std::cout << "" << _name << ":   " << (theResult.passed ? "passed" : "failed") << std::endl;
    for (TestError err: theResult.getErrors())
    {
        std::cout << err.getStackTrace();
    }
}

bool UT::wasRan()
{
    return this->_ran;
}

TestResult UT::getResult()
{
    if (this->_ran)
    {
        return this->_result;
    }
    else
    {
        throw "This test was not ran yet!";
    }
}

void UT::fail(TestError err)
{
    this->_result.addError(err);
}

void UT::assertTrue(bool sth)
{
    if (!sth)
    {
        std::stringstream ss;
        ss << "Assertion error:"  << "  expected: true"  << "  actual: false";
        fail(TestError(ss.str()));
    }
}
void UT::assertFalse(bool sth)
{
    if (sth)
    {
        std::stringstream ss;
        ss << "Assertion error:"  << "  expected: false"  << "  actual: true";
        fail(TestError(ss.str()));
    }
}
void UT::assertTrue(bool sth, std::string err)
{
    if (!sth)
    {
        std::stringstream ss;
        ss << err;
        fail(TestError(ss.str()));
    }
}
void UT::assertFalse(bool sth, std::string err)
{
    if (sth)
    {
        std::stringstream ss;
        ss << err;
        fail(TestError(ss.str()));
    }
}
void UT::assertTrue(bool sth, std::string filename, unsigned short lineNum)
{
    if (!sth)
    {
        std::stringstream ss;
        ss << "[" << filename << ":" << lineNum << "] " << "Assertion error:"  << "  expected: true"  << "  actual: false";
        fail(TestError(ss.str()));
    }
}
void UT::assertFalse(bool sth, std::string filename, unsigned short lineNum)
{
    if (sth)
    {
        std::stringstream ss;
        ss << "[" << filename << ":" << lineNum << "] " << "Assertion error:"  << "  expected: false"  << "  actual: true";
        fail(TestError(ss.str()));
    }
}
void UT::assertTrue(bool sth, std::string err, std::string filename, unsigned short lineNum)
{
    if (!sth)
    {
        std::stringstream ss;
        ss << "[" << filename << ":" << lineNum << "] " << err;
        fail(TestError(ss.str()));
    }
}
void UT::assertFalse(bool sth, std::string err, std::string filename, unsigned short lineNum)
{
    if (sth)
    {
        std::stringstream ss;
        ss << "[" << filename << ":" << lineNum << "] " << err;
        fail(TestError(ss.str()));
    }
}
