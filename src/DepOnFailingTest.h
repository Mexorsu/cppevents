#ifndef DEPONFAILINGTESTUT_H
#define DEPONFAILINGTESTUT_H
#include"UT.h"

class DepOnFailingTestUT: public UT
{
public:
    DepOnFailingTestUT();
    ~DepOnFailingTestUT();
private:
    void test();
};
#endif /* DEPONFAILINGTESTUT_H */
