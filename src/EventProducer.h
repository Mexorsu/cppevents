
#ifndef _SZYREK_EVENT_CONSUMER
#define _SZYREK_EVENT_CONSUMER
#include"EventManager.h"
#include<string>

class EventProducer
{
public:
    EventProducer();
    ~EventProducer();
    void method1();
    int method2();
    char method3();
    int costam;
    std::string costamInnego;
    template <class T> void fireEvent(T event);
private:

};
#endif
