#include<vector>

class Identifiable
{
public:
    Identifiable();
    virtual ~Identifiable();
    static long long int getNextID();
    long long int getID();
private:
    static long long int _lastID; 
    long long int _ID;
    static std::vector<long long int> _freed;
};
