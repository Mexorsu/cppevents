#ifndef _MY_GUARD_TEST_RESULT
#define _MY_GUARD_TEST_RESULT
#include"TestError.h"
#include<vector>

class TestResult
{
public:
	static TestResult TEST_OK;
    bool passed = true;
    bool failed = false;
    void addError(TestError err);
    void addAllErrors(TestResult r);
    std::vector<TestError> getErrors();
private:
    std::vector<TestError> _errors;
};
#endif
