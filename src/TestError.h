#ifndef _MYGUARD_TEST_ERROR
#define _MYGUARD_TEST_ERROR
#include<string>
#include <memory>
class TestError
{
    public:
        TestError(std::string description, TestError cause); 

        TestError(std::string description);
        std::string getDescription();
        std::shared_ptr<TestError> getCause();
        void setCause(TestError another);
        std::string getStackTrace();
        std::string getStackTrace(unsigned short depth);
    private:
        std::string _description;
        std::shared_ptr<TestError> _cause;
        bool _caused = false;
};
#endif
