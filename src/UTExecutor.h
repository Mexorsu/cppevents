#ifndef UTEXECUTOR_H
#define UTEXECUTOR_H
#include<vector>
#include<memory>
#include"UT.h"

class UTExecutor
{
public:
    static std::shared_ptr<UTExecutor> getInstance();
    void registerTest(std::shared_ptr<UT> test);
    void run();
    ~UTExecutor();
    UTExecutor();

private:
    static std::shared_ptr<UTExecutor> _instance;
    std::vector<std::shared_ptr<UT>> _tests;
};
#endif /* UTEXECUTOR_H */
