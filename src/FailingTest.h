#ifndef FAILINGTESTUT_H
#define FAILINGTESTUT_H 
#include"UT.h"
class FailingTestUT: public UT
{
public:
    FailingTestUT();
    ~FailingTestUT();
private:
    void test();
};
#endif /* FAILINGTESTUT_H */
