#include"UTExecutor.h"


std::shared_ptr<UTExecutor> UTExecutor::_instance;

UTExecutor::UTExecutor()
{

}
UTExecutor::~UTExecutor()
{
}
void UTExecutor::registerTest(std::shared_ptr<UT> test)
{
    this->_tests.push_back(test);
}
void UTExecutor::run()
{
    for (auto test: _tests)
    {
        test->run();
        test->printResults();

    }
}

std::shared_ptr<UTExecutor> UTExecutor::getInstance()
{
    if (_instance==nullptr)
    {
        _instance = std::shared_ptr<UTExecutor>();
    }
    return _instance;
}
