#!/bin/bash
APP_NAME="CPPEvents"
COMPILER_CMD="g++"
OUTPUT_DIR="bin"
FLAGS=("-std=c++11" "-Wall")
LIBS=("pthread")
SOURCE_DIRS=("src" "tests")
INCLUDE_DIRS=("src" "tests")
LIBRARY_DIRS=()
SRC_INCLUDES=("*.cpp")
SRC_EXCLUDES=("Identifiable.cpp" "EventManager.cpp" "EventProducer.cpp")
custom_clean(){
    echo_info "sooo clean"
}
before_build(){
    echo_info "Pre-compiling tests"
    . $BUILD_DIR/preCompileTests.sh $BUILD_DIR/tests
    find $BUILD_DIR/tests -name *.cpp >> $OUTPUT_DIR/.jb/files_list
    cat $OUTPUT_DIR/.jb/files_list | sort | uniq >  $OUTPUT_DIR/.jb/files_list_tmp
    mv $OUTPUT_DIR/.jb/files_list_tmp $OUTPUT_DIR/.jb/files_list
    echo_info "Registering tests"
    TEST_CPP_FILE=`find $SRC_PATHS_STRING -name Test.cpp`
    find $BUILD_DIR/tests -name '*?.cpp' | sed -e 's#^.*/\([^/]\+\)$#\1#' > $BUILD_DIR/tests.list
    echo "#include\"UTExecutor.h\"" > $TEST_CPP_FILE
    while read CPP_TEST_FILE
    do
        echo_verbose "  registering: ${CPP_TEST_FILE/.cpp/}"
        TEST_HEADER=`echo $CPP_TEST_FILE | sed -e 's/^\([^.]\+\)\.cpp/\1.h/'`
        echo "#include\"$TEST_HEADER\"" >> $TEST_CPP_FILE
    done < $BUILD_DIR/tests.list
    echo "" >> $TEST_CPP_FILE
    echo "int main(int argc, char* argv[])" >> $TEST_CPP_FILE
    echo "{" >> $TEST_CPP_FILE
    echo "    UTExecutor exec;" >> $TEST_CPP_FILE
    while read CPP_TEST_FILE
    do
        TEST_NAME=`echo $CPP_TEST_FILE | sed -e 's/^\([^.]\+\)\.cpp/\1/'`
        echo "    exec.registerTest(std::make_shared<$TEST_NAME>());" >> $TEST_CPP_FILE
    done < $BUILD_DIR/tests.list
    echo "    exec.run();" >> $TEST_CPP_FILE
    echo "    return 0;" >> $TEST_CPP_FILE
    echo "}" >> $TEST_CPP_FILE
}
after_build(){
    while read FILE
    do
        rm ${OUTPUT_DIR}/${FILE/.cpp/.obj}
    done < $BUILD_DIR/tests.list
    rm $BUILD_DIR/tests.list
    echo_info "Removing pre-compiled tests"
    find $BUILD_DIR/tests -name '*.cpp' -exec rm {} \;
    echo_info "Running tests"
    $ARTIFACT
}
