#include"TestsTest.h"
#include "DepOnFailingTest.h"
#include "DepOnPassingTest.h"
#include "PassingTest.h"
#include "FailingTest.h"

TestsTest::TestsTest() :UT("TestsTest")
{
}
TestsTest::~TestsTest()
{
}

void TestsTest::test()
{
assertTrue(true, "True asserted to false, that can't be good..", "TestsTest.ut", 16);
assertFalse(false, "True asserted to false, that can't be good..", "TestsTest.ut", 17);
auto t1=std::make_shared<DepOnFailingTestUT>();
t1->run();
assertTrue(t1->getResult().failed, "TestsTest.ut", 20);
assertFalse(t1->getResult().passed, "TestsTest.ut", 21);
auto t2=std::make_shared<PassingTestUT>();
t2->run();
assertFalse(t2->getResult().failed, "TestsTest.ut", 24);
assertTrue(t2->getResult().passed, "TestsTest.ut", 25);
auto t3=std::make_shared<DepOnPassingTestUT>();
t3->run();
assertFalse(t3->getResult().failed, "TestsTest.ut", 28);
assertTrue(t3->getResult().passed, "TestsTest.ut", 29);
auto t4=std::make_shared<FailingTestUT>();
t4->run();
assertTrue(t4->getResult().failed, "TestsTest.ut", 32);
assertFalse(t4->getResult().passed, "TestsTest.ut", 33);
}
