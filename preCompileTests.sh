#!/bin/bash

cd $1
find . -name '*.ut' > .test_list_tmp
while read FILE_NAME
do
    LINENUM=1
    rm -rf ${FILE_NAME/.ut/.cpp}
    while read LINE
    do
        FNS=`basename $FILE_NAME`
        TEST_NAME=${FNS/.ut/}
        THAT_PART=", \"$FNS\", $LINENUM"
        if [[ $LINE =~ assert(True|False|Equals).* ]]
        then
            echo $LINE | sed -e "s/\(assert\(True\|False\|Equals\)[ \t]*(.\+\)).*/\1$THAT_PART);/" >> ${FILE_NAME/.ut/.cpp}
        elif [[ $LINE =~ $TEST_NAME::$TEST_NAME() ]]
        then
            echo $LINE | sed -e "s/\($TEST_NAME::$TEST_NAME()\)/\1 :UT(\"$TEST_NAME\")/" >> ${FILE_NAME/.ut/.cpp}
        elif [[ $LINE =~ $TEST_NAME::$TEST_NAME ]]
        then
            echo $LINE | sed -e "s/\($TEST_NAME::$TEST_NAME(.*\))\([ \t{]*\)\$/\1, \"$TEST_NAME\")\2/" >> ${FILE_NAME/.ut/.cpp}
        else
            echo "$LINE" >> ${FILE_NAME/.ut/.cpp}
        fi
        LINENUM=$((LINENUM+1))
    done < $FILE_NAME
done < .test_list_tmp
cd - 2>/dev/null 1>&2

#| xargs sed = | sed 'N;s/\n//' | sed -e 's/^\([09]\+\)\([^09]\)\?\([ \t]*assert\(True\|False\)[ \t]*(.\+\));[ \t]*$/\2\3, \1);/' | sed -e 's/^\([09]\+\)\([^09]\)\?/\2/'
